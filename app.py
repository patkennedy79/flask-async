from flask import Flask
import asyncio
from aiohttp import ClientSession
from threading import current_thread
from timeit import default_timer
from functools import wraps
import requests


# Create the Flask application
app = Flask(__name__)

# List of (10) websites to access in this script
urls = ['https://www.kennedyrecipes.com',
        'https://www.kennedyrecipes.com/breakfast/pancakes/',
        'https://www.kennedyrecipes.com/breakfast/honey_bran_muffins/',
        'https://www.kennedyrecipes.com/breakfast/acai_bowl/',
        'https://www.kennedyrecipes.com/breakfast/breakfast_scramble/',
        'https://www.kennedyrecipes.com/breakfast/pumpkin_donuts/',
        'https://www.kennedyrecipes.com/breakfast/waffles/',
        'https://www.kennedyrecipes.com/breakfast/omelette/',
        'https://www.kennedyrecipes.com/baked_goods/bagels/',
        'https://www.kennedyrecipes.com/baked_goods/irish_soda_bread/']

print(f"Global scope of Flask application: {current_thread().name}")


##############
# Decorators #
##############

def timer(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start_time = default_timer()
        response = f(*args, **kwargs)
        total_elapsed_time = default_timer() - start_time
        response += f'<h3>Elapsed time: {total_elapsed_time}</h3>'
        return response
    return wrapper


####################
# Helper Functions #
####################

async def fetch_url(session, url):
    """Fetch the specified URL using the aiohttp session specified."""
    response = await session.get(url)
    return {'url': response.url, 'status': response.status}


async def get_all_urls():
    """Retrieve the list of URLs asynchronously using aiohttp."""
    async with ClientSession() as session:
        tasks = []
        for url in urls:
            task = asyncio.create_task(fetch_url(session, url))
            tasks.append(task)
        results = await asyncio.gather(*tasks)

    return results


async def load_user_from_database():
    """Mimics a long-running operation to load a user from an external database."""
    app.logger.info('Loading user from database...')
    await asyncio.sleep(1)


async def log_request_status():
    """Mimics a long-running operation to log the request status."""
    app.logger.info('Logging status of request...')
    await asyncio.sleep(1)


async def log_error():
    """Mimics a long-running operation to log the error."""
    app.logger.info('Logging status of error...')
    await asyncio.sleep(1)


#####################
# Request Callbacks #
#####################

@app.before_request
async def app_before_request():
    await load_user_from_database()


@app.after_request
async def app_after_request(response):
    await log_request_status()
    return response


##################
# Error Handlers #
##################

@app.errorhandler(404)
async def page_not_found(e):
    await log_error()
    return '<h1>Page Not Found</h1>', 404


##########
# Routes #
##########

@app.route('/')
def index():
    return 'Examples of using the new asynchronous features of Flask 2.0!'


@app.route('/async_get_urls_v1')
@timer
def async_get_urls_v1():
    """Asynchronously retrieve the list of URLs (works in Flask 1.1.x when using threads)."""
    print(f'Inside async_get_all_urls(): {current_thread().name}')

    sites = asyncio.run(get_all_urls())

    # Generate the HTML response
    response = '<h1>URLs:</h1>'
    for site in sites:
        response += f"<p>URL: {site['url']} --- Status Code: {site['status']}</p>"
    return response


@app.route('/async_get_urls_v2')
async def async_get_urls_v2():
    """Asynchronously retrieve the list of URLs using the new async/await support in Flask 2.0."""
    start_time = default_timer()
    print(f'Inside async_get_urls(): {current_thread().name}')

    # Retrieve all the URLs asynchronously using aiohttp
    async with ClientSession() as session:
        tasks = []
        for url in urls:
            task = asyncio.create_task(fetch_url(session, url))
            tasks.append(task)
        sites = await asyncio.gather(*tasks)

    # Generate the HTML response
    response = '<h1>URLs:</h1>'
    for site in sites:
        response += f"<p>URL: {site['url']} --- Status Code: {site['status']}</p>"
    total_elapsed_time = default_timer() - start_time
    response += f'<h3>Elapsed time: {total_elapsed_time}</h3>'

    return response


@app.route('/sync_get_all_urls')
@timer
def sync_get_all_urls():
    """Synchronously retrieve the list of URLs (works in Flask 1.x and 2.0)."""
    print(f'Inside sync_get_all_urls(): {current_thread().name}')

    sites = []
    for url in urls:
        response = requests.get(url)
        sites.append({'url': response.url, 'status': response.status_code})

    # Generate the HTML response
    response = '<h1>URLs:</h1>'
    for site in sites:
        response += f"<p>URL: {site['url']} --- Status Code: {site['status']}</p>"
    return response
