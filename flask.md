One of the great aspects of Flask is that it can be used for so many types of web applications.  There are lots of different solutions available for using Flask depending on the type of web application that is being developed:

| Type | Deployment Solution  | Worker Type                        | Examples |
| ------------------------------ | -------------------- | ---------------------------------- | ---------------------------- |
| Low-Volume of Requests         | Gunicorn/uWSGI             | Threads                     | Blog (dynamic content), Small/Medium-sized Web App, Low-Volume API |
| High-Volume of Requests        | Gunicorn/uWSGI             | Gevent/Eventlet                     | Medium/High-Volume API |
| Numerous I/O Calls Per Request | Gunicorn/uWSGI             | Threads/Gevent/Eventlet using `async`/`await` | Web Application with Multiple External API Calls per Request, Web Application with Multiple Database Accesses per Request |
| CPU-intensive tasks            | Gunicorn/uWSGI             | Threads plus [Celery](https://testdriven.io/courses/flask-celery/)          | Machine-learning app, image processing, etc. |
| WebSocket App | Gunicorn/uWSGI | Gevent/Eventlet | Client/Server communication app; HTML over WebSockets |
| Static Website                 | Hosted Solution (Netlify) | N/A | [Static Sites](https://testdriven.io/blog/static-site-flask-and-netlify/) (blog, marketing site, recipe site, etc.) |
| Development | Flask Development Server | Threads (*default*) | Developing Flask apps |
