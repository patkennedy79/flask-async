## Overview

This Flask application demonstrates how to utilize `async`/`await` in a Flask application for:

1. View functions
1. Callback requests
1. Error Handlers

This project utilizes Flask 2.0 (with async support), which can be installed using:

```sh
$ pip install "Flask[async]"
```

This project is utilized in the following blog post on [TestDriven.io](https://testdriven.io/blog/):

[Async in Flask 2.0](https://testdriven.io/blog/flask-async/)

## Installation Instructions

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask-async.git
```

Create a new virtual environment:

```sh
$ cd flask-async
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

Set the file that contains the Flask application and specify that the development environment should be used:

```sh
(venv) $ export FLASK_APP=app.py
(venv) $ export FLASK_ENV=development
```

Run development server to serve the Flask application:

```sh
(venv) $ flask run
```

Navigate to 'http://localhost:5000/async_get_urls_v1' or 'http://localhost:5000/async_get_urls_v2' to access (10) URLs asynchronously.

## Key Python Modules Used
 
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - web framework (*my favorite!!!*)
* [asyncio](https://docs.python.org/3/library/asyncio.html) - library writing asynchronous code using `async`/`await`
* [aiohttp](https://docs.aiohttp.org/en/stable/) - libary for creating asynchronous HTTP clients/servers
* [pytest](https://docs.pytest.org/en/6.2.x/) - test framework and test runner
* [pytest-asyncio](https://pypi.org/project/pytest-asyncio/) - pytest extension for testing asyncio code

This application is written using Python 3.9.2.

## Troubleshooting

If you run into SSL issues using aiohttp, try running the 'Install Certificates' command that came with your Python installation.

More details in the second answer of this Stack Overflow question: [SSL Certificate Verify Failed](https://stackoverflow.com/questions/27835619/urllib-and-ssl-certificate-verify-failed-error)

## Testing

To run all the tests:

```sh
(venv) $ pytest -v
```
