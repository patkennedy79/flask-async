import pytest
from app import app


############
# Fixtures #
############

@pytest.fixture(scope='module')
def test_client():
    # Create a test client using the Flask application
    with app.test_client() as testing_client:
        # Establish an application context before accessing the logger
        with app.app_context():
            app.logger.info('Creating database tables in test_client fixture...')

        yield testing_client  # this is where the testing happens!
