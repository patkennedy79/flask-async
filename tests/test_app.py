"""
This file (test_app.py) contains the functional tests for *app.py*.
"""
import pytest
import aiohttp
from app import get_all_urls, fetch_url


def test_get_homepage(test_client):
    """
    GIVEN a Flask test client
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Examples of using the new asynchronous features of Flask 2.0!' in response.data


def test_async_get_urls_v1(test_client):
    """
    GIVEN a Flask test client
    WHEN the '/async_get_urls_v1' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/async_get_urls_v1')
    assert response.status_code == 200
    assert b'URLs' in response.data


def test_async_get_urls_v2(test_client):
    """
    GIVEN a Flask test client
    WHEN the '/async_get_urls_v2' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/async_get_urls_v2')
    assert response.status_code == 200
    assert b'URLs' in response.data


@pytest.mark.asyncio
async def test_get_all_urls():
    """
    GIVEN an `asyncio` event loop
    WHEN the `get_all_urls()` coroutine is called
    THEN check that the response is valid
    """
    results = await get_all_urls()
    assert len(results) > 0
    print(type(results))
    for item in results:
        assert 'www.kennedyrecipes.com' in str(item['url'])
        assert int(item['status']) == 200


@pytest.mark.asyncio
async def test_fetch_url():
    """
    GIVEN an `asyncio` event loop
    WHEN the `fetch_url()` coroutine is called
    THEN check that the response is valid
    """
    async with aiohttp.ClientSession() as session:
        result = await fetch_url(session, 'https://www.kennedyrecipes.com/baked_goods/bagels/')

    assert str(result['url']) == 'https://www.kennedyrecipes.com/baked_goods/bagels/'
    assert int(result['status']) == 200
